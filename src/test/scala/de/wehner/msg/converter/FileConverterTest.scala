package de.wehner.msg.converter

import java.io.File
import java.time.Instant

import de.wehner.msg.convert.FileConverter
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.{Matchers => ShouldMatchers}

class FileConverterTest extends AnyFlatSpec with ShouldMatchers with MockFactory  {
  "Name extractor" should "extract the filename from path" in {
    val file = new File("./16/64/00064-16 00162 SBET Gutachter.msg")

    FileConverter.filenameFromFile(file) shouldBe "2016_00162 Gutachter.msg"
  }

  "Aktenzeichen extractor" should "extract from the file path" in {
    val file = new File("./16/64/00064-16 00162 SBET Gutachter.msg")

    FileConverter.aktenzeichenFromFile(file) shouldBe "2016-00064"
  }

  "Aktenzeichen from string" should "extract properly" in {
    FileConverter.aktenzeichenFromString("00064-16 00162 SBET Gutachter.msg") shouldBe "2016-00064"
  }

  "Instant converter" should "perform properly" in {
    FileConverter.timestampFromFilename("00064-16 00005 SBET Gutachter.msg") shouldBe Instant.parse("2016-01-01T05:00:00Z")
  }
}
