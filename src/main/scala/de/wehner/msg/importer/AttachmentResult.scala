package de.wehner.msg.importer

import java.time.Instant

import spray.json.RootJsonFormat

case class AttachmentResult(
                             migrationInfo: MigrationInfo,
                             foreignParentId: String,
                             foreignAktenId: String,
                             deleted: Boolean,
                             important: Boolean,
                             fileName: String,
                             mimeType: String,
                             size: Long,
                             path: String,
                             angelegtAm: Instant,
                             geandertAm: Instant
                           )

object AttachmentResult {
  import spray.json.DefaultJsonProtocol._
  import FileResult._

  implicit val format: RootJsonFormat[AttachmentResult] = jsonFormat11(apply)
}