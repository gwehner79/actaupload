package de.wehner.msg.importer

import java.time.LocalDateTime

import spray.json.{JsString, JsValue, RootJsonFormat}

case class MigrationInfo(
                        ignore: Boolean,
                        validated: Boolean,
                        migrated: Boolean,
                        foreignId: String,
                        lastChanged: LocalDateTime
                        )

object MigrationInfo {
  import spray.json.DefaultJsonProtocol._

  implicit val format: RootJsonFormat[MigrationInfo] = jsonFormat5(apply)

  implicit object DateJsonFormat extends RootJsonFormat[LocalDateTime] {

    override def write(obj: LocalDateTime): JsValue = {
      JsString(obj.toString)
    }

    override def read(json: JsValue): LocalDateTime = {
      LocalDateTime.parse(json.toString())
    }
  }
}


