package de.wehner.msg.importer

import java.time.Instant

import spray.json.{JsString, JsValue, RootJsonFormat}

case class FileResult(
                        migrationInfo: MigrationInfo,
                        foreignAktenId: String,
                        deleted: Boolean,
                        important: Boolean,
                        fileName: String,
                        mimeType: String,
                        size: Long,
                        path: String,
                        angelegtAm: Instant,
                        geandertAm: Instant,
                        dokumente: Option[Seq[AttachmentResult]]
                     )

object FileResult {
  import spray.json.DefaultJsonProtocol._

  implicit val format: RootJsonFormat[FileResult] = jsonFormat11(apply)

  implicit object InstantFormatter extends RootJsonFormat[Instant] {
    override def read(json: JsValue): Instant = {
      Instant.parse(json.toString())
    }

    override def write(obj: Instant): JsValue = {
      JsString(obj.toString)
    }
  }
}