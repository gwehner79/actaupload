package de.wehner.msg.convert

import java.io.File
import java.time.Instant
import java.time.temporal.{ChronoUnit, TemporalUnit}

import com.auxilii.msgparser.{Message, MsgParser}
import org.apache.commons.io.FilenameUtils

object FileConverter {
  def timestampFromFilename(filename: String) : Instant = {
    val jahr = getJahrFromFilename(filename)
    val lfdNr = lfdNrFromString(filename)

    val theInstant = Instant.parse(s"${jahr}-01-01T00:00:00Z").plus(lfdNr.toInt, ChronoUnit.HOURS)
    println(theInstant)
    theInstant
  }

  def filenameFromFile(file: File): String = {
    val splitted = file.getName.split(" ").toList
    val firsts = splitted.take(3)
    val rest = splitted.drop(3)

    s"20${file.getParentFile.getParentFile.getName}_${firsts(1)} ${rest.mkString(" ")}"
  }

  def lfdNrFromString(filename: String): _root_.scala.Predef.String = {
    filename.split(" ")(1)
  }

  def lfdNrFromFile(file: File): String = {
    lfdNrFromString(file.getName)
  }

  def filenameFromString(fileName: String): String = {
    val splitted = fileName.split(" ").toList
    val firsts = splitted.take(3)
    val rest = splitted.drop(3)
    val jahr = s"20${firsts(0).split("-")(1)}"

    s"${jahr} ${firsts(1)} ${rest.mkString(" ")}"
  }

  def aktenzeichenFromString(filename: String): String = {
    val firstElement = filename.split(" ").toList.take(1)(0)
    val aklist = firstElement.split("-").reverse
    s"20${aklist(0)}-${aklist(1)}"
  }

  def getRealString(string: String): String = {
    string
      .replaceAll("u╠ê", "ü")
      .replaceAll("o╠ê", "ö")
      .replaceAll("a╠ê", "ä")
  }

  def getJahrFromFilename(filename: String): String = {
    s"20${filename.split(" ")(0).split("-")(1)}"
  }

  def aktenzeichenFromFile(file: File): String = {
    val padded = f"${file.getParentFile.getName.toInt}%05d"
    s"20${file.getParentFile.getParentFile.getName}-${padded}"
  }

  def htmlFilenameForOld(file: File) = {
    FilenameUtils.getBaseName(filenameFromString(getRealString(file.getName))) + ".html"
  }

  def bodyFromFile(file: File): String = {
    val msgParser = new MsgParser
    val parsed = msgParser.parseMsg(file)

    var body: String = ""

    if(parsed.getBodyHTML != null) {
      body = parsed.getBodyHTML
    } else if(parsed.getBodyRTF != null) {
      body = parsed.getConvertedBodyHTML
    } else if (parsed.getBodyText != null) {
      body = parsedToHtml(parsed)
    }

    body
  }


  def parsedToHtml(parsed: Message): String = {
    val message = parsed.getBodyText
    val subject = parsed.getSubject

    val fromRenderer = {
      s"${parsed.getFromName} &lt;${parsed.getFromEmail}&gt;"
    }
    val toRender =
      s"${parsed.getToName} &lt;${parsed.getToEmail}&gt;"
    val renderMsg =
    {
      message.replaceAll("\r\n", "\n").replaceAll("\n", "<br />")
    }

    s"<!DOCTYPE html><html><head></head><body>From: $fromRenderer<br/>To: $toRender<br/>Subject: $subject<br/><br/>$renderMsg</body></html>"
  }


}
