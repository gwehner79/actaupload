package de.wehner.msg

import java.io.{BufferedOutputStream, File, FileOutputStream}
import java.nio.file.{Files, Paths}
import java.time.{Instant, LocalDateTime}
import java.util.UUID._

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.file.scaladsl.Directory
import akka.stream.scaladsl.Sink
import com.auxilii.msgparser._
import com.auxilii.msgparser.attachment._
import com.typesafe.config.ConfigFactory
import de.wehner.msg.convert.FileConverter
import de.wehner.msg.importer.{AttachmentResult, FileResult, MigrationInfo}
import org.apache.commons.io.FilenameUtils

import scala.collection.JavaConverters._
import scala.collection.immutable
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

object Boot extends App {
  val config = ConfigFactory.load()

  implicit val system: ActorSystem = ActorSystem("msgParser", config)
  implicit val mat: ActorMaterializer = ActorMaterializer.create(system)
  implicit val ec: ExecutionContextExecutor = system.dispatcher

  val msgParser = new MsgParser()
  // angelegtAm, geandertAm
  // Instants

  val pathSource = Directory.walk(Paths.get(".", config.getString("app.import.folder")), Some(3))

  val start = LocalDateTime.now()
  val eventuallyFileResults: Future[immutable.Seq[FileResult]] = {
    pathSource
      .map(_.toFile)
      .filterNot(_.isDirectory)
      .filterNot(_.isHidden)
      .filterNot(_.getName == ".DS_Store")
      .filter(file => FilenameUtils.getExtension(file.getName) == "msg")
      .mapAsync(config.getInt("app.import.fileParalelism"))(fileToResult)
      .runWith(Sink.seq)
  }
  eventuallyFileResults.onComplete {
    case Success(value) => {
      println("got list")
    }
    case Failure(exception) => exception.printStackTrace()
  }


  eventuallyFileResults.foreach(resultList => {
    resultList.foreach(result => {
      println(result)
    })
  })




  def saveFile(destinationFile: File, bytes: Array[Byte]): File = {
    val bos = new BufferedOutputStream(new FileOutputStream(destinationFile, false))
    Stream.continually(bos.write(bytes))
    bos.close()
    destinationFile
  }

  def saveAttachment(attachment: Attachment, mainUUID: String, aktenId: String, lfdNr: String, jahr: String, savePath: String, saveTimestamp: Instant): AttachmentResult = {
    if (attachment.isInstanceOf[FileAttachment]) {
      val fileAttachment = attachment.asInstanceOf[FileAttachment]
      val attachmentUUID = randomUUID().toString

      val newFilename = s"${jahr} ${lfdNr} ${fileAttachment.getFilename}"
      val newFileSavePath = Paths.get(savePath, newFilename).toString
      AttachmentResult(
        migrationInfo = MigrationInfo(
          false,
          false,
          false,
          attachmentUUID,
          LocalDateTime.now
        ),
        foreignParentId = mainUUID,
        foreignAktenId = aktenId,
        deleted = false,
        important = false,
        fileName = newFilename,
        mimeType = fileAttachment.getMimeTag,
        size = fileAttachment.getSize,
        path = newFileSavePath,
        angelegtAm = saveTimestamp,
        geandertAm = saveTimestamp
      )
    } else {
      throw new Exception("bla")
    }
  }


  def fileToResult(file: File): Future[FileResult] = {
    Future({
      val filename = file.getName
      val parsed = msgParser.parseMsg(file)
      val fileBasename = FilenameUtils.getBaseName(filename)
      val fileExtension = FilenameUtils.getExtension(filename)
      val mainFileUuid = randomUUID().toString

      val body = FileConverter.bodyFromFile(file)
      val bodyBytes = body.getBytes("UTF-8")

      val relativePathString = file.getParentFile.getPath

      val newFileName = FileConverter.htmlFilenameForOld(file)
      val fileSavePath = Paths.get(relativePathString, newFileName)

      // Save the file
      //      saveFile(fileSavePath.toFile, bodyBytes)

      val aktenzeichen = FileConverter.aktenzeichenFromString(file.getName)
      val timestamp = FileConverter.timestampFromFilename(file.getName)
      var result = FileResult(
        migrationInfo = MigrationInfo(
          ignore = false,
          validated = false,
          migrated = false,
          foreignId = mainFileUuid,
          lastChanged = LocalDateTime.now()
        ),
        foreignAktenId = aktenzeichen,
        deleted = false,
        important = false,
        fileName = newFileName,
        mimeType = Files.probeContentType(fileSavePath),
        size = file.length(),
        path = fileSavePath.toString,
        dokumente = None,
        geandertAm = timestamp,
        angelegtAm = timestamp
      )


      if (parsed.getAttachments.size() > 0) {
        result.copy(dokumente = Some(parsed.getAttachments.asScala.toList.map(attachment => {
          saveAttachment(
            attachment = attachment,
            mainUUID = mainFileUuid,
            aktenId = aktenzeichen,
            lfdNr = FileConverter.lfdNrFromFile(file),
            jahr = FileConverter.getJahrFromFilename(file.getName),
            savePath = relativePathString,
            saveTimestamp = timestamp
          )
        })))
      } else {
        result
      }
    })
  }
}
