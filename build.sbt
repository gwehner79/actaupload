val AkkaVersion = "2.5.31"

name := "msg"

version := "0.1"

scalaVersion := "2.12.11"

libraryDependencies ++= Seq (
  "com.auxilii.msgparser" % "msgparser" % "1.1.15",
  "com.lightbend.akka" %% "akka-stream-alpakka-file" % "2.0.0",
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe" % "config" % "1.4.0",
  "io.spray" %% "spray-json" % "1.3.5",
  "commons-io" % "commons-io" % "2.7"
)

libraryDependencies += "org.scalactic" %% "scalactic" % "3.1.2"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.2" % Test
libraryDependencies += "org.scalamock" %% "scalamock" % "4.4.0" % Test